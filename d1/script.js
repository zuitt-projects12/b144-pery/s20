// JSON Objects
/*
	JSON stands for JavaScript Object Notation. JSON is used for serializing different data types into bytes.

	Serialization is the process of converting data into a series of bytes for easier transmission/transfer of information.

	Byte = binary digits (1, 0) that is used to represent a character or letters. It is the language of the computer.
*/

/*
	JSON data format
	Syntax: 
	{
		"propertyA": "valueA",
		"propertyB": "valueB"
		.
		.
		.
	}
*/

/*
	JS Object
	{
		city: "Pasig",
		province: "Metro Manila",
		country: "PH"
	}

	JSON
	{
		"city": "Pasig",
		"province": "Metro Manila",
		"country": "PH"
	}
	
	JSON array

	"cities": [
		{
			"city": "Pasig",
			"province": "Metro Manila",
			"country": "PH"
		},
		{
			"city": "QC",
			"province": "Metro Manila",
			"country": "PH"
		},
		{
			"city": "Mandaluyong City",
			"province": "Metro Manila",
			"country": "PH"
		}
	]

*/

// JSON Methods
/*
	The JSON contains methods for parsing and converting data into stringified JSON.
*/

/*
	Stringified JSON is a JavaScript object converted into a string to be used in other functions of a Javascript application.
*/

let batchesArr = [
	{
		batchName: "Batch X"
	},
	{
		batchName: "Batch Y"
	}
];

console.log(`Result from stringify method`);

// The "stringify" method is used to convert JS Objects into JSON(string)
console.log(JSON.stringify(batchesArr));


let data = JSON.stringify({
	name: "Jon",
	age: 16,
	address: {
		house: "Stark",
		location: "Winterfell"
	}
});

console.log(data);

/*let firstName = prompt("First Name: ");
let lastName = prompt("Last Name: ");
let age = prompt("Age ");
let address = [prompt("City: "), prompt("Country: "), prompt("Zip Code: ")];


let userDetails = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
});

console.log(userDetails);*/

// Converting stringified JSON into JS objects.

/*
	Information is commonly sent to applications in stringified JSON and converted back to obejcts.

	This happens both for sending information to a backend app and sending information back to front-end app.

	Upon receiving data, JSON text can be converted to a JS object with parse()
*/

let batchesJSON = `[
	{
		"batchname": "Batch X"
	},
	{
		"batchname": "Batch Y"
	}
]`;

console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `
	{
		"name": "Jon Snow",
		"age": 16,
		"address": {
			"house": "Stark",
			"location": "Winterfell"
		}
	}
`;

console.log(JSON.parse(stringifiedObject));

