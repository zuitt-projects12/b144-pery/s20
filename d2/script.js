function msgToSelf(){
	console.log("DON'T TEXT HER BACK!!!");
}
let count = 10;
while(count !== 0){
	msgToSelf();
	count--;
}

count = 1;
while(count <= 5){
	console.log(count);
	count++;
}

/*Do-while loop

	Do while loop is similar to the while loop. However, do while will allow us to run over loop at least once.
*/

/*
	while - we check the condition first before running our codeblock.

	do-while - it will do an instruction first before it will check the condition to run again.
*/

count = 1;
do{
	console.log(count);
	count++;
}while(count <= 20)

for(count = 0; count < 10; count++){
	console.log(count);
}

let fruits = ["Apples", "Mango", "Pineapple", "Guyabano"];

// show all the items in an array in the console

for(count = 0; count < fruits.length; count++){
	console.log(fruits[count]);
}

for(count = fruits.length - 1; count > -1; count--){
	console.log(fruits[count]);
}

let favoriteCountries = ["Finland","Norway","Iceland","Switzerland","Sweden","Denmark"];

for(let count = 0; count < favoriteCountries.length; count++){
	console.log(favoriteCountries[count]);
}

// Global/local scope

/*
	Global scoped variables are variables that can be accessed inside a function or anywhere in our script
*/

let username = "super_knight_1000";

function sample(){
	/*
		Local scoped variable cannot be accesed outside the function it was created from.
	*/
	let username = "thebinsubtleissubtle";
	let heroName = "One Punch Man";
	console.log(username);
	console.log(heroName);
}

sample();

console.log(username);

let powerpuffgirls = ["Blossom", "Bubbles", "Buttercup"];

console.log(powerpuffgirls[0][2]);

let name = "Alexandro";

console.log(name[8]);
console.log(name[name.length - 1]);

function counter(str){
	for(let i = 0; i < str.length; i++){
		console.log(str[i]);
	}
}

counter(name);